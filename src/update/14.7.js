export default [{
    avatar: 'jla',
    info: [{
        type: 'cs',
        from: 110,
        to: 105,
        status: -1,
    }],
}, {
    avatar: 'mgn',
    info: [{
        type: 'sc',
        from: 90,
        to: 95,
        status: 1,
    }],
}, {
    avatar: 're',
    info: [{
        type: 'cs',
        from: 105,
        to: 100,
        status: 1,
    }],
}, {
    avatar: 'wh',
    info: [{
        type: 'cd',
        from: '-20',
        to: '-0',
        status: 1,
    }],
}, {
    avatar: 'lj',
    info: [{
        type: 'cs',
        from: 110,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'qn',
    info: [{
        type: 'sc',
        from: 95,
        to: 100,
        status: 1,
    }],
}, {
    avatar: 'tm',
    info: [{
        type: 'sc',
        from: 85,
        to: 90,
        status: 1,
    }],
}, {
    avatar: 'ejt',
    info: [{
        type: 'cs',
        from: 110,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'jl2',
    info: [{
        type: 'cd',
        from: '-20',
        to: '-0',
        status: 1,
    }],
}, {
    avatar: 'dcz',
    info: [{
        type: 'cs',
        from: 105,
        to: 110,
        status: -1,
    }],
}, {
    avatar: 'jl',
    info: [{
        type: 'cs',
        from: 105,
        to: 110,
        status: -1,
    }],
}];