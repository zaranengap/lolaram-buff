export default [{
    avatar: 'yn',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'hb',
    info: [{
        type: 'sc',
        from: 85,
        to: 90,
        status: 1,
    }],
}, {
    avatar: 'w',
    info: [{
        type: 'cd',
        from: '0',
        to: '+10',
        status: 1,
    }],
}, {
    avatar: 'ky',
    info: [{
        type: 'rx',
        from: '0',
        to: '+20',
        status: 1,
    }],
}, {
    avatar: 'ez',
    info: [{
        type: 'sc',
        from: 100,
        to: 95,
        status: -1,
    }],
}, {
    avatar: 'tt',
    info: [{
        type: 'cs',
        from: 108,
        to: 110,
        status: -1,
    }],
}, {
    avatar: 'wjj',
    info: [{
        type: 'cs',
        from: 85,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'ey',
    info: [{
        type: 'sc',
        from: 105,
        to: 100,
        status: -1,
    }],
}, {
    avatar: 'rz',
    info: [{
        type: 'cs',
        from: 90,
        to: 95,
        status: -1,
    }],
}, {
    avatar: 'qn',
    info: [{
        type: 'zl',
        from: '-10',
        to: '-15',
        status: -1,
    }, {
        type: 'hd',
        from: '-10',
        to: '-15',
        status: -1,
    }],
}];