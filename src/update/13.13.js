export default [{
    avatar: 'jm2',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'nj',
    info: [{
        type: 'sc',
        from: 95,
        to: 100,
        status: 1,
    }],
}, {
    avatar: 'jks2',
    info: [{
        type: 'sc',
        from: 90,
        to: 95,
        status: 1,
    }],
}, {
    avatar: 'szm',
    info: [{
        type: 'hd',
        from: '+0',
        to: '+10',
        status: 1,
    }],
}, {
    avatar: 'hj',
    info: [{
        type: 'sc',
        from: 105,
        to: 100,
        status: -1,
    }],
}, {
    avatar: 'lb',
    info: [{
        type: 'sc',
        from: 105,
        to: 100,
        status: -1,
    }],
}, {
    avatar: 'qyn',
    info: [{
        type: 'cs',
        from: 80,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'gf',
    info: [{
        type: 'cs',
        from: 85,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'gf',
    info: [{
        type: 'cs',
        from: 85,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'j',
    info: [{
        type: 'cs',
        from: 90,
        to: 95,
        status: -1,
    }],
}];