export default [{
    avatar: 'bly',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }, {
        type: 'zl',
        from: '+15',
        to: '+20',
        status: 1,
    }],
}, {
    avatar: 'tl',
    info: [{
        type: 'sc',
        from: 105,
        to: 110,
        status: 1,
    }],
}, {
    avatar: 'yj',
    info: [{
        type: 'sc',
        from: 105,
        to: 110,
        status: 1,
    }],
}, {
    avatar: 'nyfl',
    info: [{
        type: 'sc',
        from: 105,
        to: 110,
        status: 1,
    }],
}, {
    avatar: 'qyn',
    info: [{
        type: 'sc',
        from: 110,
        to: 115,
        status: 1,
    }],
}, {
    avatar: 'szg',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'zm',
    info: [{
        type: 'cs',
        from: 92,
        to: 100,
        status: -1,
    }],
}];