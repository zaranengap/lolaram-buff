export default [{
    avatar: 'xknh',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'ky',
    info: [{
        type: 'gs',
        from: '0',
        to: '+2.5',
        status: 1,
    }],
}, {
    avatar: 'xp',
    info: [{
        type: 'cs',
        from: 100,
        to: 95,
        status: 1,
    }],
}, {
    avatar: 'tn',
    info: [{
        type: 'cs',
        from: 105,
        to: 100,
        status: 1,
    }],
}, {
    avatar: 'sml',
    info: [{
        type: 'cs',
        from: 105,
        to: 100,
        status: 1,
    }],
}, {
    avatar: 'vn',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'dm',
    info: [{
        type: 'cd',
        from: '0',
        to: '+20',
        status: 1,
    }],
}, {
    avatar: 'lw',
    info: [{
        type: 'cd',
        from: '0',
        to: '-20',
        status: -1,
    }],
}, {
    avatar: 'xxg',
    info: [{
        type: 'sc',
        from: 100,
        to: 95,
        status: -1,
    }],
}, {
    avatar: 'pk',
    info: [{
        type: 'cs',
        from: 85,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'ky',
    info: [{
        type: 'sc',
        from: 105,
        to: 100,
        status: -1,
    }],
}, {
    avatar: 'bd',
    info: [{
        type: 'cs',
        from: 80,
        to: 85,
        status: -1,
    }],
}, {
    avatar: 'jla',
    info: [{
        type: 'cs',
        from: 105,
        to: 110,
        status: -1,
    }],
}, {
    avatar: 'yj',
    info: [{
        type: 'sc',
        from: 110,
        to: 105,
        status: -1,
    }],
}, {
    avatar: 'hb',
    info: [{
        type: 'cd',
        from: '-20',
        to: '-30',
        status: -1,
    }, {
        type: 'cs',
        from: 115,
        to: 100,
        status: 1,
    }]
}];