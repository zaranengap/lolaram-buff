export default [{
    avatar: 'qj',
    info: [{
        type: 'gs',
        from: '0',
        to: '+2.5',
        status: 1,
    }],
}, {
    avatar: 'nyfl',
    info: [{
        type: 'cs',
        from: 100,
        to: 95,
        status: 1,
    }],
}, {
    avatar: 'yn',
    info: [{
        type: 'sc',
        from: 103,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'lw',
    info: [{
        type: 'cd',
        from: '-20',
        to: '0',
        status: 1,
    }],
}, {
    avatar: 'bly',
    info: [{
        type: 'zl',
        from: '+30',
        to: '+20',
        status: -1,
    }],
}, {
    avatar: 'jks2',
    info: [{
        type: 'sc',
        from: 95,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'mgn',
    info: [{
        type: 'sc',
        from: 94,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'mlo',
    info: [{
        type: 'hd',
        from: '-5',
        to: '-10',
        status: -1,
    }],
}, {
    avatar: 'xr',
    info: [{
        type: 'zl',
        from: '+20',
        to: '+10',
        status: -1,
    }],
}, {
    avatar: 'ez',
    info: [{
        type: 'zl',
        from: '+20',
        to: '+10',
        status: -1,
    }],
}];