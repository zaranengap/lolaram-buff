export default [{
    avatar: 'hb',
    info: [{
        type: 'sc',
        from: 90,
        to: 100,
        status: 1,
    }],
}, {
    avatar: 'rw',
    info: [{
        type: 'hd',
        from: '+20',
        to: '+0',
        status: -1,
    }],
}];