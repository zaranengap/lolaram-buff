export default [{
    avatar: 'hui',
    info: [{
        type: 'cd',
        from: '+20',
        to: '+10',
        status: -1,
    }],
}, {
    avatar: 'nm',
    info: [{
        type: 'sc',
        from: 100,
        to: 95,
        status: -1,
    }, {
        type: 'zl',
        from: '-5',
        to: '-10',
        status: -1,
    }],
}, {
    avatar: 'smd',
    info: [{
        type: 'cs',
        from: 100,
        to: 105,
        status: -1,
    }, {
        type: 'cd',
        from: '-0',
        to: '-10',
        status: -1,
    }],
}];