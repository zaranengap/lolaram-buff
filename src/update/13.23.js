export default [{
    avatar: 'ksd',
    info: [{
        type: 'cs',
        from: 100,
        to: 95,
        status: 1,
    }],
}, {
    avatar: 'nl',
    info: [{
        type: 'zl',
        from: '-10',
        to: '-0',
        status: 1,
    }],
}, {
    avatar: 'x',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'kst',
    info: [{
        type: 'cs',
        from: 95,
        to: 100,
        status: -1,
    }],
}, {
    avatar: 'lb',
    info: [{
        type: 'cs',
        from: 95,
        to: 100,
        status: -1,
    }],
}, {
    avatar: 'tm',
    info: [{
        type: 'cs',
        from: 95,
        to: 100,
        status: -1,
    }],
}, {
    avatar: 'hb',
    info: [{
        type: 'cd',
        from: '-30',
        to: '-0',
        status: 1,
    }],
}];