export default [{
    avatar: 'rm',
    info: [{
        type: 'cs',
        from: 95,
        to: 90,
        status: 1,
    }],
}, {
    avatar: 'ns',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'lxa',
    info: [{
        type: 'cd',
        from: '0',
        to: '+10',
        status: 1,
    }],
}, {
    avatar: 'amm',
    info: [{
        type: 'cs',
        from: 105,
        to: 100,
        status: 1,
    }],
}, {
    avatar: 'fj',
    info: [{
        type: 'cd',
        from: '0',
        to: '+10',
        status: 1,
    }],
}, {
    avatar: 'ps',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'bn2',
    info: [{
        type: 'cs',
        from: 100,
        to: 105,
        status: -1,
    }],
}, {
    avatar: 'jm2',
    info: [{
        type: 'cs',
        from: 95,
        to: 100,
        status: -1,
    }],
}, {
    avatar: 'lj',
    info: [{
        type: 'cs',
        from: 105,
        to: 110,
        status: -1,
    }],
}, {
    avatar: 'tm',
    info: [{
        type: 'sc',
        from: 90,
        to: 85,
        status: -1,
    }],
}, {
    avatar: 'jl2',
    info: [{
        type: 'cd',
        from: '0',
        to: '-20',
        status: -1,
    }],
}, {
    avatar: 'gt',
    info: [{
        type: 'sc',
        from: 95,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'xl',
    info: [{
        type: 'cs',
        from: 105,
        to: 110,
        status: -1,
    }],
}, {
    avatar: 'wy',
    info: [{
        type: 'zl',
        from: '-10',
        to: '-20',
        status: -1,
    }],
}, {
    avatar: 'pk',
    info: [{
        type: 'cs',
        from: 90,
        to: 95,
        status: -1,
    }],
}];