export default [{
    avatar: 'fh',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'jks',
    info: [{
        type: 'sc',
        from: 100,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'ls',
    info: [{
        type: 'cs',
        from: 100,
        to: 95,
        status: 1,
    }],
}, {
    avatar: 'lr',
    info: [{
        type: 'zl',
        from: '+0',
        to: '+5',
        status: 1,
    }],
}, {
    avatar: 'akl',
    info: [{
        type: 'cs',
        from: 85,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'dcz',
    info: [{
        type: 'cs',
        from: 100,
        to: 105,
        status: -1,
    }],
}, {
    avatar: 'aw',
    info: [{
        type: 'hd',
        from: '+0',
        to: '-20',
        status: -1,
    }, {
        type: 'sc',
        from: 95,
        to: 90,
        status: -1,
    }],
}, {
    avatar: 'mezh',
    info: [{
        type: 'sc',
        from: 92,
        to: 90,
        status: -1,
    }, {
        type: 'cs',
        from: 108,
        to: 110,
        status: -1,
    }],
}, {
    avatar: 'aybb',
    info: [{
        type: 'sc',
        from: 100,
        to: 95,
        status: -1,
    }],
}];