export default [{
    avatar: 'yn',
    info: [{
        type: 'cs',
        from: 100,
        to: 95,
        status: 1,
    }],
}, {
    avatar: 'xl',
    info: [{
        type: 'cs',
        from: 110,
        to: 105,
        status: 1,
    }],
}, {
    avatar: 'kst',
    info: [{
        type: 'cs',
        from: 90,
        to: 95,
        status: -1,
    }],
}, {
    avatar: 'mz',
    info: [{
        type: 'zl',
        from: '+40',
        to: '+35',
        status: -1,
    }],
}];